import React from "react";
import { FaSearch } from "react-icons/fa";
import Header_handle from "./Header_handle";
export default function Header_nav_center() {
  return (
    <div className="flex border border-gray-300 rounded-full relative">
      <div className="cursor-pointer p-3 font-semibold px-4 border-r border-gray-300">
        Địa điểm bất kỳ
      </div>
      <div className="cursor-pointer p-3 font-semibold px-4 border-r border-gray-300">
        Tuần bất kỳ
      </div>
      <div className="cursor-pointer flex items-center pl-4 pr-1 text-slate-500">
        <p className="pr-4">Thêm Khách</p>
        <div
          className="p-3 rounded-full"
          style={{ color: "white", background: "#FF385C" }}
        >
          <FaSearch />
        </div>
      </div>
      <div>
        <Header_handle />
      </div>
    </div>
  );
}
