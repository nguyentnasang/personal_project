import React, { useEffect, useRef, useState } from "react";
import { TbWorld } from "react-icons/tb";
import { FaBars, FaUserAlt } from "react-icons/fa";
export default function Header_nav_right() {
  const ref = useRef();
  console.log("ref", ref.current);
  const [display, setDisplay] = useState("none");
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  useEffect(() => {
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        setDisplay("none");
      }
    }

    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref]);
  const handleLoginClick = () => {
    setIsLoggedIn(!isLoggedIn);
    setDisplay(isLoggedIn ? "none" : "block");
  };

  return (
    <div className="flex justify-center items-center">
      <div className="p-3 font-semibold">Cho thuê chỗ ở qua Airbnb</div>
      <div className="p-3 text-xl">
        <TbWorld />
      </div>
      {/* //phần đăng nhập */}
      <div
        onClick={handleLoginClick}
        className="relative cursor-pointer flex justify-center items-center pl-3 py-1 pr-1 border border-gray-300 rounded-full"
      >
        <FaBars />
        <div
          style={{ background: "#666f7a", color: "#495966" }}
          className="ml-3 rounded-full p-2"
        >
          {" "}
          <FaUserAlt />
        </div>
        {/* phần hiển thị đăng nhập */}
        <div
          ref={ref}
          style={{ bottom: "-295px", display: display }}
          className="overflow-hidden absolute right-0 bg-white z-10 border border-gray-300 w-56 text-left rounded-xl loginonf"
        >
          <div className="  border-b-2 border-gray-300">
            <p className="pl-3 py-1.5 hover:bg-slate-300 font-semibold">
              Tin nhắn
            </p>
            <p className="pl-3 hover:bg-slate-300 font-semibold py-1.5">
              Chuyến đi
            </p>
            <p className="pl-3 py-1.5 hover:bg-slate-300 font-semibold">
              Danh sách yêu thích
            </p>
          </div>
          <div className="   border-b-2 border-gray-300">
            <p className="pl-3 hover:bg-slate-300 py-1.5">cho thuê chỗ</p>
            <p className="pl-3 hover:bg-slate-300 py-1.5">
              tổ chức trải nghiệm
            </p>
            <p className="pl-3 hover:bg-slate-300 py-1.5">tài khoản</p>
          </div>
          <div className="">
            <p className="pl-3 hover:bg-slate-300 py-1.5">Trợ giúp</p>
            <p className="pl-3 hover:bg-slate-300 py-1.5">Đăng xuất</p>
          </div>
        </div>
      </div>
    </div>
  );
}
