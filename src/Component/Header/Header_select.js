import React, { Component } from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
export default class Header_select extends Component {
  render() {
    const settings = {
      //   dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 5,
      slidesToScroll: 3,
    };
    return (
      <div className=" mr-20 mt-5">
        <Slider {...settings}>
          <div className="flex justify-center items-center ">
            <img
              className="w-6 m-auto"
              src={require("../../img/Header/khungcanhtuyetvoi.jpg")}
              alt=""
            />
            <h3>Khung cảnh tuyệt vời</h3>
          </div>
          <div className="flex justify-center items-center ">
            <img
              className="w-6 m-auto"
              src={require("../../img/Header/nhanho.jpg")}
              alt=""
            />
            <h3>Nhà nhỏ</h3>
          </div>
          <div className="flex justify-center items-center ">
            <img
              className="w-6 m-auto"
              src={require("../../img/Header/thietke.jpg")}
              alt=""
            />
            <h3>Thiết kế</h3>
          </div>
          <div className="flex justify-center items-center ">
            <img
              className="w-6 m-auto"
              src={require("../../img/Header/container.jpg")}
              alt=""
            />
            <h3>Container</h3>
          </div>
          <div className="flex justify-center items-center ">
            <img
              className="w-6 m-auto"
              src={require("../../img/Header/laudai.jpg")}
              alt=""
            />
            <h3>Lâu đài</h3>
          </div>
          <div className="flex justify-center items-center ">
            <img
              className="w-6 m-auto"
              src={require("../../img/Header/phongrieng.jpg")}
              alt=""
            />
            <h3>Phòng Riêng</h3>
          </div>
          <div className="flex justify-center items-center ">
            <img
              className="w-6 m-auto"
              src={require("../../img/Header/bietthu.jpg")}
              alt=""
            />
            <h3>Biệt Thự</h3>
          </div>
          <div className="flex justify-center items-center ">
            <img
              className="w-6 m-auto"
              src={require("../../img/Header/nhakhongsudungcacdichvutienich.jpg")}
              alt=""
            />
            <h3>Nhà không sử dụng các dịch vụ</h3>
          </div>
          <div className="flex justify-center items-center ">
            <img
              className="w-6 m-auto"
              src={require("../../img/Header/huongbien.jpg")}
              alt=""
            />
            <h3>Hướng biển</h3>
          </div>{" "}
          <div className="flex justify-center items-center ">
            <img
              className="w-6 m-auto"
              src={require("../../img/Header/thatantuong.jpg")}
              alt=""
            />
            <h3>Thật ấn tượng</h3>
          </div>{" "}
          <div className="flex justify-center items-center ">
            <img
              className="w-6 m-auto"
              src={require("../../img/Header/duocuachuong.jpg")}
              alt=""
            />
            <h3>Được ưa chuộng</h3>
          </div>{" "}
          <div className="flex justify-center items-center ">
            <img
              className="w-6 m-auto"
              src={require("../../img/Header/nhanongtrai.jpg")}
              alt=""
            />
            <h3>Nhà nông trại</h3>
          </div>{" "}
          <div className="flex justify-center items-center ">
            <img
              className="w-6 m-auto"
              src={require("../../img/Header/khonggiansangtao.jpg")}
              alt=""
            />
            <h3>Không gian sáng tạo</h3>
          </div>{" "}
          <div className="flex justify-center items-center ">
            <img
              className="w-6 m-auto"
              src={require("../../img/Header/leumucdong.jpg")}
              alt=""
            />
            <h3>Lều mục đồng</h3>
          </div>{" "}
          <div className="flex justify-center items-center ">
            <img
              className="w-6 m-auto"
              src={require("../../img/Header/vuonnho.jpg")}
              alt=""
            />
            <h3>Vườn nho</h3>
          </div>{" "}
          <div className="flex justify-center items-center ">
            <img
              className="w-6 m-auto"
              src={require("../../img/Header/huongbien.jpg")}
              alt=""
            />
            <h3>Hướng biển</h3>
          </div>{" "}
          <div className="flex justify-center items-center ">
            <img
              className="w-6 m-auto"
              src={require("../../img/Header/choigolf.jpg")}
              alt=""
            />
            <h3>Chơi golf</h3>
          </div>{" "}
        </Slider>
      </div>
    );
  }
}
