import React from "react";
import Header_nav_center from "./Header_nav_center";
import Header_nav_right from "./Header_nav_right";
import Header_select from "./Header_select";
import { GrFilter } from "react-icons/gr";
export default function Header() {
  return (
    <div className="container m-auto mb-10">
      <div className="  flex justify-between items-center mt-2">
        <img
          className="py-3"
          width={118}
          // height={64}
          src={require("../../img/Header/logo.png")}
          alt=""
        />
        <div className="flex flex-grow justify-center lg:ml-60">
          {" "}
          <Header_nav_center />
        </div>
        <div>
          <Header_nav_right />
        </div>
      </div>
      <div className="relative">
        <div className="">
          <Header_select />
        </div>
        <div className="cursor-pointer p-2 absolute top-0 right-0 flex justify-center items-center border border-gray-300 rounded-full">
          <GrFilter />
          <p className="ml-2">Bộ lọc</p>
        </div>
      </div>
    </div>
  );
}
